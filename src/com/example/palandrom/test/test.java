package com.example.palandrom.test;


public class test {
	public boolean outcome;
	public String helper;
	
	public int results (int number){
		System.out.println( "this is the Edit Text" + number);
		
		if (number < 1000000) {
			
			System.out.println("The Number should be 7 digits");
			outcome = false;
			helper = "Please choose a number that is at least 7 digits";
			
			
		} else if (number > 9999999) {
			System.out.println("The Number is lager than 7 digits");
			outcome = false;
			helper = "Please choose a number that is only 7 digits.";
		} else {
			//Break down the number to check if it is a Palindrom
			int firstNumber = number % 10;
			int secondNumber = (number / 10) % 10;
			int thirdNumber = (number / 100) % 10;
			int fourthNumber = (number / 1000) % 10;
			int fifthNumber = (number / 10000) % 10;
			int sixthNumber = (number / 100000) % 10;
			int seventhNumber = (number / 1000000) % 10;
			
			//Reodering the number
			int reorderedNumber = (firstNumber * 1000000) + (secondNumber * 100000) +(thirdNumber * 10000) + (fourthNumber * 1000) + (fifthNumber * 100) + (sixthNumber * 10) + seventhNumber;
			
			// Test to see if number is Palindrom
			if (reorderedNumber == number) {
				outcome = true;
				System.out.println("The Number is a Palindrom.");
			}else {
				System.out.println("The Number is not a Palindrom");
				outcome = false;
				helper = "Please choose a number that is a Palindrom";
				
			}
			
			
		}
		return number;
		
	}

}
