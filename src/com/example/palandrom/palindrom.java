package com.example.palandrom;

import com.example.palandrom.test.test;

import android.R.bool;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class palindrom extends Activity {
	
	EditText number;
	TextView help;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.palindrom);
		
		final test result = new test();
		
		
		
		Button testButton = (Button) findViewById(R.id.buttonTest);
		testButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// Loads button and calls test class
				number = (EditText) findViewById(R.id.editPalindrom);
				help = (TextView) findViewById(R.id.textHelper);
				
				int realNumber = Integer.parseInt(number.getText().toString());
				result.results(realNumber);
				boolean outcome = result.outcome;
				String helper = result.helper;
				System.out.println("The outcome is " + outcome + " and the helper is :" + helper);
				//find result and output or move to next screen
				if (outcome == false) {
					number.clearComposingText();
					help.setText(helper);
					System.out.println(result.helper);
				} else if (outcome == true) {
					startActivity(new Intent(palindrom.this, youdidit.class));
					
				}
				
				
				
			
				
				
				
			}
		});
	}

}
